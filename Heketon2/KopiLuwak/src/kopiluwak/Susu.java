/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kopiluwak;

/**
 *
 *@warungkopiluwakjantan
 */
public class Susu extends CondimentDecorator {
    Kopi kopi;
    
    public Susu(Kopi kopi){
        this.kopi = kopi;
    }
    
    public String getDescription(){
        return kopi.getDescription() + ", Susu";
    }
    
    public double cost(){
        return 2000 + kopi.cost();
    }
}
